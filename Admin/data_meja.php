<?php
include 'header.php'
?>
  <?php 
include 'database.php';
$db = new database();
?>
 <div class="content">
        <div class="header">
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Meja</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div><br>
              <div class="btn-toolbar list-toolbar">
                  <a href="#tambah_user" data-toggle="modal" >  <button class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button></a>
             <div class="row"><br>
                   <div class="col-md-12">
                       <div class="white-box">
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Meja</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                          
                                          
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                    <?php
                                    error_reporting(0);
                                    $no = 1;
                                    foreach($db->tampil_data_meja() as $x){
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['no_meja']; ?></td>
                                             <td>
                                         <?php
                                            if($data['status_meja'] == 'Y')
                                            {
                                              ?>
                                            <a href="approver.php?table=meja&id_meja=<?php echo $data['id_meja']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Tersedia
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approver.php?table=meja&id_meja=<?php echo $data['id_meja']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Penuh
                                            </a>
                                            <?php
                                            }
                                            ?>
                                       
                                        </td>
                                          
                                       
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                   </table>
                            </div>
                       </div>
                   </div>
            
<?php
include 'footer.php'
?>
