<?php

include 'koneksi.php';
require('../pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('../css/images/3.jpg',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'Healthy Food Restaraun',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0038XXXXXXX',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. pajajaran',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.malasngoding.com email : malasngoding@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,0.7,'Laporan Data Transaksi ',0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->Cell(6,0.7,"Transaksi pada : ".$_GET['tanggal'],0,0,'C');
$pdf->ln(1);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Masakan', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Harga', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Quantity', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Keterangan', 1, 0, 'C');
$pdf->Cell(4.5, 0.8, 'Status', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Jumlah', 1, 1, 'C');

$no=1;
$tanggal=$_GET['tanggal'];
$query=mysql_query("SELECT *,sum(detail_order.jumlah) as jml from detail_order INNER JOIN masakan ON masakan.id_masakan= detail_order.id_masakan where detail_order.id_order='$_GET[id_order]' group by detail_order.id_masakan where tanggal=" . $tanggal);

while($lihat=mysql_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama_masakan'],1, 0, 'C');
	$pdf->Cell(4, 0.8, "Rp. ".number_format($lihat['harga'])." ,-", 1, 0,'C');
	$pdf->Cell(4.5, 0.8, "Rp. ".number_format($lihat['jumlah'])." ,-",1, 0, 'C');
$pdf->Cell(3, 0.8, $lihat['keterangan'], 1, 0,'C');
	
	$pdf->Cell(3, 0.8, $lihat['status_detail_order'], 1, 0,'C');
	$pdf->Cell(4.5, 0.8, "Rp. ".number_format($lihat['jumlah'])." ,-",1, 0, 'C');
	
	$no++;
}
$q=mysql_query("select sum(total_harga) as total from detail_order where tanggal=".$tanggal);
// select sum(total_harga) as total from barang_laku where tanggal='$tanggal'
while($total=mysql_fetch_array($q)){
	$pdf->Cell(17, 0.8, "Total Pendapatan", 1, 0,'C');		
	$pdf->Cell(4.5, 0.8, "Rp. ".number_format($total['total'])." ,-", 1, 0,'C');	
}
$qu=mysql_query("select sum(laba) as masakan from user where tanggal=".$tanggal);
// select sum(total_harga) as total from barang_laku where tanggal='$tanggal'
while($tl=mysql_fetch_array($qu)){
	$pdf->Cell(4, 0.8, "Rp. ".number_format($tl['total_laba'])." ,-", 1, 1,'C');	
}
$pdf->Output("laporan_buku.pdf","I");

?>

