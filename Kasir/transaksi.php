
<?php
include'database.php';
$db = new database();
?>
<?php
include 'header.php'
?>

<div class="content">
        <div class="header">
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Order </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No. Meja</th>
                                            <th>Tanggal</th>
                                            <th>Nama User</th>
                                            <th>Keterangan</th>
                                            <th>Status Order</th>
                                            <th>Keterangan Pembayaran</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    <?php
                                    error_reporting(0);
                                    $no = 1;
                                    foreach($db->tampil_pesan_keterangan() as $x){
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['no_meja']; ?></td>
                                            <td><?php echo $x['tanggal']; ?></td>
                                            <td><?php echo $x['nama_user']; ?></td>
                                            <td><?php echo $x['keterangan']; ?></td>
                                            <td><?php
                                            if($x['status_order'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "diterima";?></td><?php }?>
                                            <td><?php
                                            if($x['keterangan_transaksi'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Terbayar";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum Terbayar";?>
                                            <?php 
                                            }
                                            ?></td>
                                           <td>
                                           <button class="btn btn-success" type="submit" data-toggle="modal"
                                                                            data-target="#myModal<?php echo $x['id_transaksi'];?>"><i class="fa fa-shopping-cart"></i> Bayar
                                                                    </button>
                                           <a href="detail_bayar?id_order=<?php echo $x['id_order']; ?>"><button type="button" class="btn btn-warning">Detail</button></a>
                                             
                                         </td>

                        <div class="modal fade" id="myModal<?php echo $x['id_transaksi'];?>" tabindex="-1" role="dialog"
                                             aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">&times;
                                                        </button>
                                  
                                                        <h4 class="modal-title"> TOTAL :
                                                            Rp. <?php echo number_format($x['total_bayar'], 0, ',', '.'); ?></h4>
                                                    </div>

                                                    <div class="modal-body row">
                                                        <div class="col-md-12">
                                                            <!--<form method="POST" action="?hal=cetak">-->
                                                                <form method="POST" action="update_transaksi.php?id_transaksi=<?php echo $x['id_transaksi'];?>">
                                                                <div class="form-group">
                                                                    <label> Cash</label>
                                                               <input type="hidden" class="form-control" value="<?php echo $x['no_meja']; ?>" name="id_meja"/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="type1" name="jumlah_uang"/>
                                                                </div>

                                                                <div class="pull-right">
                                                                    <button class="btn btn-primary btn-sm"
                                                                            type="submit"><i
                                                                                class="fa fa-check-square-o"></i> OK
                                                                    </button>
                                                                    <button class="btn btn-danger btn-sm"
                                                                            data-dismiss="modal" aria-hidden="true"
                                                                            type="button"><i class="fa fa-times"></i>
                                                                        Cancel
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


               </tr>
                 <?php 
           }
           ?>
                                        </tr>
                                    </tbody>
                                   </table>
                            </div>
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


        <!--Start  Footer -->
<?php 
include 'footer.php'
?>
    <!--End Page Level Plugin-->
   


