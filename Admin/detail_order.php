<?php
include "header.php";
?>

<?php
include'../database.php';
$db = new database();
?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Detail Order
          </h1>
        </section>
<?php
include "../koneksi1.php";

$edit = mysqli_query($conn, "SELECT * FROM detail_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where id_order='$_GET[id_order]'");
$r=mysqli_fetch_array($edit);
?>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <a href="data_pesan.php" class="btn btn-danger">Kembali</a>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="tester" class="table">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Pesanan</th>
                         <th>Harga</th>
                          <th>Jumlah</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                      
                      </tr>
                    </thead>
                    <?php
include "../koneksi1.php";
$no=1;
$total_bayar=0;
$data = "SELECT * FROM detail_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where id_order='$_GET[id_order]'";
$bacadata = mysqli_query($conn, $data);
while($r= mysqli_fetch_array($bacadata))
{
?>
                    <tbody>
                      <tr>
                        <td><?php echo $no++ ;?></td>
                        <td><?php echo $r['nama_masakan'];?></td>
                        <td><?php echo $r['harga'];?></td>
                        <td><?php echo $r['jumlah'];?></td>
                        <td><?php echo $r['keterangan'];?></td>
                        <td><?php echo $r['jumlah']*$r['harga'];?></td>
                      </tr>
                    </tbody>

<?php 
$total_bayar += ($r['jumlah']*$r['harga']);
} 
?>
                   <tr>
                    <td colspan="6" align="right"><h4><b>Total</b></h4></td>
                    <td><h4><?php echo $total_bayar;?></h4></td>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
  <?php
include "footer.php";
?>
</body>
</html>
