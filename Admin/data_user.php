            <?php
            include "header.php";
            ?> 
            <?php 
            include 'database.php';
            $db = new database();
            ?>
            <div class="content">
              <div class="header">

                <h1 class="page-title">Data Pengguna</h1>
                <ul class="breadcrumb">
                  <li><a href="index.php">Dashboard</a> </li>
                  <li class="active">Data Pengguna</li>
                </ul>

              </div>


              <div class="main-content">

                <div class="btn-toolbar list-toolbar">
                  <a href="#tambah_user" data-toggle="modal" >  <button class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button></a>

                  <div class="btn-group">
                  </div>
                </div>
                <table id="tester" class="table">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Nama User</th>
                      <th>Id Level</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Aksi</th>

                    </tr>
                  </thead>
                  <tbody>

                     <?php
                                   error_reporting(0);
                                      $no = 1;
                                      foreach($db->tampil_data() as $data){
                                      ?>
                      <tr class="active">
                        <td><?php echo $no++; ?></center></td>
                        <td><?php echo $data['username']; ?></td>
                        <td><?php echo $data['password']; ?></td>
                        <td><?php echo $data['nama_user']; ?></td>
                        <td><?php echo $data['id_level']; ?></td>
                        <td><?php echo $data['email']; ?></td>
                        <td>
                         <?php
                         if($data['status'] == 'Y')
                         {
                          ?>
                          <a href="approve.php?table=user&id_user=<?php echo $data['id_user']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                            Aktif
                          </a>
                          <?php
                        }else{
                          ?>

                          <a href="approve.php?table=user&id_user=<?php echo $data['id_user']; ?>&action=verifed" class="btn btn-danger btn-md">
                            Tidak Aktif
                          </a>
                          <?php
                        }
                        ?>
                      </td>
                      <td>
                       
                          <a href="edit_hak.php?id_user=<?php echo $data['id_user'];?>" button  class="btn btn-primary"><i class="fa fa-edit "></i> Edit</button></a>

                          </td></tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                    <a href="proses.php">  <button class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button></a>
                  <div class="modal fade" id="tambah_user" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">      
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <div class="center">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                            <h1 class="page-title">Tambah Data</h1>
                            <ul class="breadcrumb">
                            </ul>

                          </div>
                          <div class="main-content">

                           <div class="row">
                              <div class="col-md-6">
                                <br>
                                <form action="proses_user.php?aksi=tambah" method="post">
                                  <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane active in" id="home">
                                      <form id="tab">

                                        <div class="form-group">
                                          <label>Username</label>
                                          <input type="text" name="username" class="form-control">
                                        </div>
                                        <div class="form-group">
                                          <label>Password</label>
                                          <input type="text" name="password"  class="form-control">
                                        </div>
                                        <div class="form-group">
                                          <label>Nama User</label>
                                          <input type="text" name="nama_user" class="form-control">
                                        </div>
                                        <div class="form-group">
                                          <label>Level</label>
                                          <select name="id_level" id="DropDownTimezone" class="form-control">
                                           <option >-Tidak Ada-</option>
                                           <option >1</option>
                                           <option >2</option>
                                           <option >3</option>
                                           <option >4</option>
                                           <option >5</option>
                                         </select>
                                         <div class="form-group">
                                          <label>Email</label>
                                          <input type="text" name="email" class="form-control">
                                        </div>
                                      </div>
                                    </form>
                                  </div>



                                  <div class="btn-toolbar list-toolbar">
                                    <button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <button class="btn btn-danger">Batal</button>
                                  </div>
                                </div>
                              </div>
                            </div></div></div></div></div></form>



                         
                          </form>
                        </div></div>


                        <?php
                        include "footer.php";
                        ?> 
