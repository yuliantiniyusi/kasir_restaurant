<?php
include "header.php";
include '../database.php';
$db = new database();
?>
<link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
     <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
         <?php
         include "../koneksi1.php";
         $query_edit = mysqli_query($conn,"SELECT * FROM pesan inner join user on pesan.id_user=user.id_user");
         $x = mysqli_fetch_array($query_edit)
         ?>
         <h3 class="box-title">Data Order Meja No <?php echo $x['no_meja']; ?></h3>
       </div><!-- /.box-header -->
       <!-- form start -->
       <form role="form">
        <div class="box-body">
          <div class="form-group">
            <label for="exampleInputEmail1">No Meja</label>
            <input class="form-control" value="<?php echo $x['no_meja']; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Nama Pelanggan</label>
            <input  class="form-control" value="<?php echo $x['nama_user']; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Tanggal</label>
            <input class="form-control" value="<?php echo $x['tanggal']; ?>" readonly>
          </div>
        </div><!-- /.box-body -->
      </form>

    </div><!-- /.box -->
  </div>

  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Orderan</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
       <div class="agile3-grids">
        <p align="left"><a href="pesan_makanan.php" class="btn btn-primary">Tambah Pesanan</a></p>
      </div>
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Masakan</th>
            <th>Harga</th>
            <th>Quantity</th>
            <th>Keterangan</th>
            <th>Status</th>
            <th>Jumlah</th>



          </tr>
        </thead>
        <?php
        $no = 1;
        $total=0;

        foreach($db->tampil_detail_order() as $x){
          ?>
          <tbody>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $x['nama_masakan']; ?></td>
              <td><?php echo $x['harga']; ?></td>
              <td><input type="text" class="form-control" name="" value="<?php echo $x['jumlah']; ?>"></td>
              <td><?php echo $x['keterangan']; ?></td>
              <td><?php echo $x['status_detail_order']; ?></td>
              <td><?php echo $x['jumlah']*$x['harga'] ; ?></td>
            </tr>
          </tbody>
          <?php 
          $total += ($x['jumlah']*$x['harga']) ;
        }
        ?>
        <tr>
          <td colspan="6" align="right"><h4><b>Total</b></h4></td>
          <td ><h4><?php echo $total;?></h4></td>

        </tr>
      </table>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div><!-- /.col -->


</div><!-- /.row -->
<!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include "footer.php";
?>