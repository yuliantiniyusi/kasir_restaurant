<?php


include '../database.php';
$db = new database();
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Healthy Food Restaurant | User</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
    <meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
    <meta name="author" content="FREEHTML5.CO" />
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Merriweather:300,400italic,300italic,400,700italic' rel='stylesheet' type='text/css'>
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Simple Line Icons -->
    <link rel="stylesheet" href="css/simple-line-icons.css">
    <!-- Datetimepicker -->
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
    <!-- Flexslider -->
    <link rel="stylesheet" href="css/flexslider.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <link rel="stylesheet" href="css/style.css">


    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
        <div class="js-sticky">
            <div class="fh5co-main-nav">
                <div class="container-fluid">
                    <div class="fh5co-menu-1">

                    </div>
                    <div class="fh5co-logo">
                        <a href="home.php">HF</a>
                    </div>

                    <div class="fh5co-menu-2">
                            <a href="#" data-nav-section="about">Home</a>
                        <a href="#" data-nav-section="menu">Order Makanan</a>

                    

                        <a href="#hasil" data-toggle="modal" data-target="#myModal2"><img width="15"  src="images/123.png"></a>
                        <span class="badge">

                             <?php
        if(isset($_SESSION['items'])){
          echo count($_SESSION['items']);
        }
        else{
          echo "0";
        }
        ?>
     
        </span>

        
                    
                    </div>

                </div>
                
            </div>
        </div>
<div id="fh5co-about" data-section="about">
            <div class="fh5co-2col fh5co-bg to-animate-2" style="background-image: url(images/3.jpg)"></div>
            <div class="fh5co-2col fh5co-text">
                <h2 class="heading to-animate">Healthy Food</h2>
                <p class="to-animate"><span class="firstcharacter">H</span>ealthy food restoran, merupakan tempat makanan yang sehat untuk tubuh</p>
               
            </div> 
        </div>

	
		<div id="fh5co-menus" data-section="menu">
			<div class="container">
				<div class="row text-center fh5co-heading row-padded">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="heading to-animate">Food Menu</h2>
						<p class="sub-heading to-animate"></p>
					</div>
				</div>
				<div class="row row-padded">
					<div class="col-md-12">
						<div class="fh5co-food-menu to-animate-2">
							<h2 class="fh5co-drinks">Drinks</h2>
											  <?php
              include'../koneksi1.php';
                                    // Include / load file koneksi.php
                                    // Cek apakah terdapat data pada page URL
                                    $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                                    $limit = 8; // Jumlah data per halamanya

                                    // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
                                    $limit_start = ($page - 1) * $limit;

                                    // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                                    $data=mysqli_query($conn, "SELECT * FROM masakan where status_masakan='Y'LIMIT ".$limit_start.",".$limit);
                                    $no = $limit_start + 1; // Untuk penomoran tabel
                                    while($show=mysqli_fetch_array($data)){
                                    ?>
							<div class="col-md-6">                              
                                <ul>
                                    <li>
                                        <div class="fh5co-food-desc">
                                            <figure>
                                                  <p><img src='../css/images/<?php echo $show['gambar']; ?>' width='150'></p>
                                            </figure>
                                            <div>
                                                <h3><?php echo $show['nama_masakan'];?></h3>

                                            </div>
                                        </div>
                                        <div class="fh5co-food-pricing">
                                         <p><?php echo 'Rp '.number_format($show['harga']);?></p>
                                          <button class="btn btn-primary" id="sa-basic"><a href ="cart.php?act=add&amp;id_masakan=<?php echo $show['id_masakan']; ?> &amp;ref=menu.php"><font color ="white">Pesan</font></a></button>
                                        </div>
                                    
                                    </li>
                                    
                                </ul>
                            </div>
								<?php } ?>
						</div>
					</div>
					
				
							
				
			</div>
		</div>

<div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center to-animate-2">
                        <p><a href="#" class="btn btn-primary btn-outline">More Food Menu</a></p>
                    </div>
                </div>
		
<div class="modal fade" id="myModal2"  role="dialog">
                 <div class="modal-content">
                                                                
<section id="portfolio" class="portfolio">
            <div class="container">
                <div class="row">
                    <div class="main_mix_content text-center sections">
                        <div class="head_title">
                            <h2>Daftar Pesanan</h2>
                        </div>
                           <p align='left'>
                <a href="menu.php"><button class="btn btn-success">Tambah Pesanan</button></a></p>
           <div class="table-responsive">
            <form action="proses_masakan.php" method="post">
            <table class="table table-bordered table-striped">
                <tr>   
               <th><center>No</center></th>
               <th><center>Nama Masakan</center></th>
               <th><center>Harga</center></th>
               <th><center>Quantity</center></th>
               <th><center>Sub Total</center></th>
               <th><center>Keterangan</center></th>
               <th><center>Cancel</center></th>
             </tr>
             <?php

             include '../koneksi1.php';
             $no = 1;
             $total = 0;
    //mysql_select_db($database_conn, $conn);
             if (isset($_SESSION['items'])) {
              foreach ($_SESSION['items'] as $key => $val) {
                $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
                $data = mysqli_fetch_array($query);
                $jumlah_barang = mysqli_num_rows($query);
                $jumlah_harga = $data['harga'] * $val;
                $total += $jumlah_harga;
                $harga= $data['harga'];
                $hasil="Rp.".number_format($harga,2,',','.');
                $hasil1="Rp.".number_format($jumlah_harga,2,',','.');
                $total1="Rp.".number_format($total,2,',','.');
                ?>
                <tr>

                  <td><center><?php echo $no++; ?></center></td>
                  <td><center><input type="hidden" name='id_masakan[]' value="<?php echo $data['id_masakan'];?>"><?php echo $data['nama_masakan']; ?></center></td>
                  <td><center> <?php echo $hasil; ?> </center></td>
                  <td><center><input type='hidden' name="jumlah[]" value="<?php echo ($val); ?>"><?php echo ($val); ?> Pcs </center></td>
                   <td><center><?php echo $hasil1; ?></center></td>
                   <td><center><textarea name="keterangan[]" ></textarea></center></td>
                   <td><center><a href="cart.php?act=del&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=menu.php">Cancel</a></center></td>
                 </tr>

                 <?php
                    //mysql_free_result($query);      
               }
              //$total += $sub;
             }?>
             <?php
             if($total == 0){ ?>
             <td colspan="7" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
             <?php } else { ?>
             <td colspan="7" style="font-size: 18px;"><b><div class="pull-right"><input type='hidden' value='<?php echo $total;?>' name='total_bayar' >Total Harga : <?php echo $total1; ?></div> </b></td>
<?php 
}
?>
         </table>
       </div>
       <p align='right'>
             <a href="#tambahuser" data-toggle="modal" class="btn btn-primary">Pesan</a>
                <div class="modal" id="tambahuser">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Pelanggan</h4>
                  </div>
                  <div class="modal-body">                    
                        <div class="box-body">
                                      <div class="form-group">
                                    <label for="no_meja">No Meja :</label>
                                    <select name="no_meja" class="form-control" required>
                                       <option>Pilih No Meja</option>
                                      <?php
                                      include'../koneksi1.php';
                                        $query = $conn->query("SELECT * FROM meja WHERE status_meja = 'Y'");
                                        $hitung = mysqli_num_rows($query);
                                        while($rows = mysqli_fetch_assoc($query)):
                                      ?>
                                        <option value="<?php echo $rows['id_meja'];?>"><?php echo $rows['no_meja'];?></option>
                                      <?php endwhile;?>
                                  </select>
                                </div>
                         
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                      <div class="form-group">
                      <?php
              if ($hitung  < 1) {
                echo"<div class='alert alert-danger' align='center'>Mohon maaf meja sedang penuh</div>";
              }
              else{
            ?>
                    <?php }?>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success"> Simpan</button>
                    </div>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
     </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
     </form>
      

   </div><!-- /.box-body -->


    </div><!-- /.box -->
</div>
        <!--Start  Footer -->
	<?php
	include 'footer.php'
	?>