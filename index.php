
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Healthy Food Restaurant | User</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Merriweather:300,400italic,300italic,400,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="Pelanggan/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="Pelanggan/css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="Pelanggan/css/simple-line-icons.css">
	<!-- Datetimepicker -->
	<link rel="stylesheet" href="Pelanggan/css/bootstrap-datetimepicker.min.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="Pelanggan/css/flexslider.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="Pelanggan/css/bootstrap.css">

	<link rel="stylesheet" href="Pelanggan/css/style.css">


	<!-- Modernizr JS -->
	<script src="Pelanggan/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>

	<div id="fh5co-container">
		<div id="fh5co-home" class="js-fullheight" data-section="home">

			<div class="flexslider">
				
				<div class="fh5co-overlay"></div>
				<div class="fh5co-text">
					<div class="container">
						<div class="row">
							<h1 class="to-animate">welcome </h1>
							<h2 class="to-animate">to Healthy Food Restaurant </h2>
							<br>
<p class="text-center to-animate"><a href="sign-in.php" class="btn btn-primary btn-outline">Login</a></p>
						</div>

					</div>
				</div>
			  	<ul class="slides">
			   	<li style="background-image: url(css/images/2.jpg);" data-stellar-background-ratio="0.5"></li>
			   	<li style="background-image: url(css/images/3.jpg);" data-stellar-background-ratio="0.5"></li>
			   	<li style="background-image: url(css/images/4.jpg);" data-stellar-background-ratio="0.5"></li>
			  	</ul>

			</div>
			
		</div>

	<!-- jQuery -->
	<script src="Pelanggan/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="Pelanggan/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="Pelanggan/js/bootstrap.min.js"></script>
	<!-- Bootstrap DateTimePicker -->
	<script src="Pelanggan/js/moment.js"></script>
	<script src="Pelanggan/js/bootstrap-datetimepicker.min.js"></script>
	<!-- Waypoints -->
	<script src="Pelanggan/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="Pelanggan/js/jquery.stellar.min.js"></script>

	<!-- Flexslider -->
	<script src="Pelanggan/js/jquery.flexslider-min.js"></script>
	<script>
		$(function () {
	       $('#date').datetimepicker();
	   });
	</script>
	<!-- Main JS -->
	<script src="Pelanggan/js/main.js"></script>

	</body>
</html>

