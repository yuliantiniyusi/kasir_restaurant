<?php

include "header.php";
include '../database.php';
$db = new database();
?>

<body class="sticky-header">


   
    <div class="content">
      <div class="header">


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Meja </h4>
                <ol class="breadcrumb">
                </ol>
                <div class="clearfix"></div>
             </div>
        </span><br>
             <div class="row"><br>
           <?php
            include'../koneksi.php';
                                    // Include / load file koneksi.php
                                    // Cek apakah terdapat data pada page URL
                                    $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                                    $limit = 8; // Jumlah data per halamanya

                                    // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
                                    $limit_start = ($page - 1) * $limit;

                                    // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                                    $data=mysqli_query($koneksi, "SELECT * FROM meja LIMIT ".$limit_start.",".$limit);
                                    $no = $limit_start + 1; // Untuk penomoran tabel
                                    while($show=mysqli_fetch_array($data)){
                                    $id_meja = $show['id_meja'];
                                    $meja = $show['no_meja'];
                                    ?>
              <div class="col-lg-3 col-xs-12">
                  <div class="white-box alert-box">
                    <input type="hidden" name="id_meja" value="<?php echo $show['id_meja'];?>">
                    <p> No. Meja : <?php echo $meja;?></p>
                     <p><img src='../css/images/<?php echo $show['gambar']; ?>' width='150'></p>
                    <button class="btn btn-primary" id="sa-basic"><a href ="cart_meja.php?act1=add&amp;id_meja=<?php echo $show['id_meja'];?> &amp;ref1=entri_order.php"><font color ="white">Pesan</font></a></button>
                </div>
              </div>
<?php } ?>
               </div>
               <div class="pagination">
               <?php
            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
            ?>
                <li class="disabled"><a href="#">First</a></li>
                <li class="disabled"><a href="#">&laquo;</a></li>
            <?php
            } else { // Jika buka page ke 1
                $link_prev = ($page > 1) ? $page - 1 : 1;
            ?>
                <li><a href="meja.php?page=1">First</a></li>
                <li><a href="meja.php?page=<?php echo $link_prev; ?>">&laquo;</a></li>
            <?php
            }
            ?>

            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $sql2 = mysqli_query($koneksi,"SELECT COUNT(*) AS jumlah FROM masakan ");
            ($get_jumlah = (mysqli_fetch_array($sql2)));

            $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <li <?php echo $link_active; ?>><a href="meja.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
            }
            ?>

            <!-- LINK NEXT AND LAST -->
            <?php
            // Jika page sama dengan jumlah page, maka disable link NEXT nya
            // Artinya page tersebut adalah page terakhir
            if ($page == $jumlah_page) { // Jika page terakhir
            ?>
                <li class="disabled"><a href="#">&raquo;</a></li>
                <li class="disabled"><a href="#">Last</a></li>
            <?php
            } else { // Jika bukan page terakhir
                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
            ?>
                <li><a href="meja.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
                <li><a href="meja.php?page=<?php echo $jumlah_page; ?>">Last</a></li>
            <?php
            }
            ?>
            </div>
        <!-- End Wrapper-->
        </div>
        <!--Start  Footer -->
   <?php
   include 'footer.php'
   ?>