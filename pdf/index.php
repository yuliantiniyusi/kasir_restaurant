
<?php
// memanggil library FPDF
require('fpdf.php');
// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A5');
// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',16);
// mencetak string 
$pdf->Cell(190,7,'Makanan sehat',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'Data Pengguna ',0,1,'C');
 
// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);
 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'Username',1,0);
$pdf->Cell(85,6,'Nama User',1,0);
$pdf->Cell(27,6,'id_level',1,0);
$pdf->Cell(25,6,'Email',1,1);
 
$pdf->SetFont('Arial','',10);
 
include '../koneksi.php';
$user = mysqli_query($koneksi, "select * from user");
while ($row = mysqli_fetch_array($user)){
    $pdf->Cell(20,6,$row['username'],1,0);
    $pdf->Cell(85,6,$row['nama_user'],1,0);
    $pdf->Cell(27,6,$row['id_level'],1,0);
    $pdf->Cell(25,6,$row['email'],1,1); 
}
 
$pdf->Output();
?>