<?php 

class database{

	var $host = "localhost";
	var $uname = "root";
	var $pass = "";
	var $db = "restoran";
	public $mysqli;

	function __construct(){
		$this->mysqli = new mysqli($this->host, $this->uname, $this->pass ,$this->db);
	}

	function tampil_data(){
		$data =$this->mysqli->query("SELECT * FROM user ");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil; 
	}

	function input($username,$password,$nama_user,$status,$id_level,$email){
		$data =$this->mysqli->query("insert into user values('','$username','$password','$nama_user','$status','$id_level','$email')");
	}	

	function hapus($id_user){
		$data = $this->mysqli->query("DELETE from user where id_user='$id_user'");
	}

	function edit($id_user){
		$data =$this->mysqli->query("SELECT * FROM user INNER JOIN level ON user.id_level = level.id_level where id_user='$id_user'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function update($id_user,$username,$password_aman,$email,$nama_user,$id_level){
		$data = $this->mysqli->query("update user set username='$username', password='$password_aman', email='$email', nama_user='$nama_user', id_level='$id_level' where id_user='$id_user'");
	}

	function hapus_masakan($id_masakan){
		$data = $this->mysqli->query("DELETE from masakan where id_masakan='$id_masakan'");
	}

	function tampil_data_kategori(){
		$data =$this->mysqli->query("SELECT * FROM kategori");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function inputkategori($nama_kategori){
		$data =$this->mysqli->query("insert into kategori values('','$nama_kategori')");
	}

	function edit_kategori($id_kategori){
		$data =$this->mysqli->query("SELECT * FROM kategori where id_kategori='$id_kategori'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function update_kategori($id_kategori,$nama_kategori){
		$data = $this->mysqli->query("UPDATE kategori set nama_kategori='$nama_kategori' where id_kategori='$id_kategori'");
	}

	function hapus_kategori($id_kategori){
		$data = $this->mysqli->query("DELETE from kategori where id_kategori='$id_kategori'");
	}

	function tampil_data_poder(){
		$data =$this->mysqli->query("SELECT pesan.id_order, pesan.no_meja, pesan.tanggal, user.nama_user,pesan.keterangan,pesan.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from pesan INNER JOIN user ON pesan.id_user = user.id_user inner join transaksi on pesan.id_order=transaksi.id_order where keterangan_transaksi='N'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function tampil_data_oder_terbayar(){
		$data =$this->mysqli->query("SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user,oder.keterangan,oder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order where keterangan_transaksi='Y'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function tampil_data_meja(){
		$data =$this->mysqli->query("SELECT * FROM meja");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function inputmeja($no_meja,$status_meja){
		$data =$this->mysqli->query("insert into meja values('','$no_meja','$status_meja')");
	}

	function update_meja($id_meja,$no_meja){
		$data = $this->mysqli->query("UPDATE meja set no_meja='$no_meja' where id_meja='$id_meja'");
	}

	function tampil_pesan_keterangan(){
		$data =$this->mysqli->query("SELECT pesan.id_order, pesan.no_meja, pesan.tanggal, user.nama_user,pesan.keterangan,pesan.status_order,transaksi.keterangan_transaksi, transaksi.id_transaksi, transaksi.total_bayar from pesan INNER JOIN user ON pesan.id_user = user.id_user inner join transaksi on pesan.id_order=transaksi.id_order where transaksi.keterangan_transaksi='N' and pesan.status_order='Y'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function detail_tampil(){

 
		$data =$this->mysqli->query("SELECT *,sum(detail_order.jumlah) as jml from detail_order INNER JOIN masakan ON masakan.id_masakan= detail_order.id_masakan where detail_order.id_order='$_GET[id_order]' group by detail_order.id_masakan");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function pesan_tampil_keterangan(){
		$data =$this->mysqli->query("SELECT pesan.id_order, pesan.no_meja, pesan.tanggal, user.nama_user,pesan.keterangan,pesan.status_order,transaksi.keterangan_transaksi, transaksi.id_transaksi, transaksi.total_bayar from pesan INNER JOIN user ON pesan.id_user = user.id_user inner join transaksi on pesan.id_order=transaksi.id_order where transaksi.keterangan_transaksi='N'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
} 
 
?>