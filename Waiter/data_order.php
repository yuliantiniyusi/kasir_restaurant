 <?php
          include "header.php";
          ?> 
          <?php 
          include '../database.php';
          $db = new database();
          ?>
<div class="content">
            <div class="header">

              <h1 class="page-title">Data Pengguna</h1>
              <ul class="breadcrumb">
                <li><a href="index.php">Dashboard</a> </li>
                <li class="active">Data Pengguna</li>
              </ul>

            </div>

            <div class="main-content">

              <div class="btn-toolbar list-toolbar">
                <a href="#tambah_user" data-toggle="modal" >  <button class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button></a>

                <div class="btn-group">
                </div>
              </div>
              <table id="tester" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id Order</th>
                    <th>No Meja</th>
                    <th>Tanggal</th>
                    <th>Id User</th>
                    <th>Keterangan</th>
                    <th>Pemesanan</th>
                    <th>Status Order</th>
                    <th>Aksi</th>

                  </tr>
                </thead>
                <tbody>

                  <?php
                  $no = 1;
                  foreach($db->tampil_pesan() as $data){
                    ?>


                    <tr class="active">
                      <td><?php echo $no++; ?></center></td>
                      <td><?php echo $data['id_order']; ?></td>
                      <td><?php echo $data['no_meja']; ?></td>
                      <td><?php echo $data['tanggal']; ?></td>
                      <td><?php echo $data['id_user']; ?></td>
                      <td><?php echo $data['keterangan']; ?></td>
                      <td>
                        <a href="data_menu.php" button type="button" class="btn btn-primary btn-flat btn-pri"><i class="fa fa-plus" aria-hidden="true"></i> Pesan</button>
</td>
                           <td>
                                         <?php
                                            if($data['status_order'] == 'Y')
                                            {
                                              ?>
                                            <a href="approveee.php?table=pesan&id_order=<?php echo $data['id_masakan']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                           Selesai
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approveee.php?table=pesan&id_order=<?php echo $data['id_masakan']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Verifikasi
                                            </a>
                                            <?php
                                            }
                                            ?>
                                       
                                        </td>
                      <td>
                        <a href="hapus_order.php?id_order=<?php echo $data['id_order'];?>" button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Delete</button></a>
                          <a href="edit_order.php?id_order=<?php echo $data['id_order'];?>"  button  class="btn btn-primary"><i class="fa fa-edit "></i> Edit</button></a>

                        </td></tr>
                        <?php
                      }
                      ?>

                    </tbody>
                  </table>
 <?php
          include "footer.php";
          ?>


