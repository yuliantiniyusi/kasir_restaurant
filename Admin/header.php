<?php
session_start();
if(!isset($_SESSION['login'])){

    header('location:../sign-in.php');
}
?> 

<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>Healthy Food Restaurant | Admin</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="../css/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="../css/lib/font-awesome/css/font-awesome.css">


<script src="../css/lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="../css/lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
 <script type="text/javascript" src="assets/js/jquery.min.js"></script>

 <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
    <script src="../css/lib/bootstrap/js/bootstrap.js"></script>
     

<link rel="stylesheet" type="text/css" href="../css/stylesheets/theme.css">
<link rel="stylesheet" type="text/css" href="../css/stylesheets/premium.css">
<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">


</head>
<body class=" theme-blue">

<!-- Demo page code -->

<script type="text/javascript">
    $(function() {
        var match = document.cookie.match(new RegExp('color=([^;]+)'));
        if(match) var color = match[1];
        if(color) {
            $('body').removeClass(function (index, css) {
                return (css.match (/\btheme-\S+/g) || []).join(' ')
            })
            $('body').addClass('theme-' + color);
        }

        $('[data-popover="true"]').popover({html: true});
        
    });
</script>
<style type="text/css">
    #line-chart {
        height:300px;
        width:800px;
        margin: 0px auto;
        margin-top: 1em;
    }
    .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
        color: #fff;
    }
</style>

<script type="text/javascript">
    $(function() {
        var uls = $('.sidebar-nav > ul > *').clone();
        uls.addClass('visible-xs');
        $('#main-menu').append(uls.clone());
    });
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">


<!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
<!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
<!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
<!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 

<!--<![endif]-->

<div class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="" href="index.php"><span class="navbar-brand"><span class="fa fa-cutlery"></span> Healthy Food Restaurant </span></a></div>

    <div class="navbar-collapse collapse" style="height: 1px;">
      <ul id="main-menu" class="nav navbar-nav navbar-right">
        <li class="dropdown hidden-xs">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span>
                 <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($koneksi, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['username']; ?>
                          <?php } ?>
    
                <i class="fa fa-caret-down"></i>
            </a>

          <ul class="dropdown-menu">
            <li><a tabindex="-1" href="logout.php"><i class="fa  fa-sign-in"></i> Logout</a></li>
       <li class="divider"></li>
                <li><a href="./"><i class="fa  fa-group"></i>  Register</a></li>
                <li class="divider"></li>
             
              </ul>
          </ul>
        </li>
      </ul>

    </div>
  </div>
</div>




 <div class="sidebar-nav">
    <ul>
       <li><a href="index.php" class="nav-header"><i class="fa fa-fw fa-dashboard"></i>Dashboard</a></li>
    
            
          
    </ul></li>
 <ul>
    <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa  fa-pencil-square-o"></i> Data Master<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
            <li><a href="data_user.php"><span class="fa fa-caret-right"></span>Data Pengguna</a></li>
            <li ><a href="data_masakan.php"><span class="fa fa-caret-right"></span>Data Masakan</a></li>
          <li ><a href="data_meja.php"><span class="fa fa-caret-right"></span>Data Meja</a></li>
    </ul></li>
    
        <li><a href="#" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-cutlery"></i>Entri Order<i class="fa fa-collapse"></i></a></li>
        <li><ul class="legal-menu nav nav-list collapse">
            <li ><a href="data_pesan.php"><span class="fa fa-caret-right"></span>Data Order</a></li>
            <li ><a href="meja.php"><span class="fa fa-caret-right"></span> Order Makanan</a></li>
           
    </ul></li>

        <li><a href="transaksi.php" class="nav-header"><i class="fa fa-money"></i> Entri Transaksi</a></li>
             <li><a href="laporan.php" class="nav-header"><i class="fa fa-book"></i> Generate Laporan</a></li>
    </div>



