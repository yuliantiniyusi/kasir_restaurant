          <?php
          include "header.php";
          ?> 
          <?php 
          include '../database.php';
          $db = new database();
          ?>
          <div class="content">
            <div class="header">

              <h1 class="page-title">Data Pengguna</h1>
              <ul class="breadcrumb">
                <li><a href="index.php">Dashboard</a> </li>
                <li class="active">Data Pengguna</li>
              </ul>

            </div>
  <?php
         include "../koneksi1.php";
         $query_edit = mysqli_query($conn,"SELECT * FROM pesan inner join user on pesan.id_user=user.id_user");
         $x = mysqli_fetch_array($query_edit)
         ?>
            <div class="main-content">

              <div class="btn-toolbar list-toolbar">
                <a href="#tambah_user" data-toggle="modal" >  <button class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button></a>

                <div class="btn-group">
                </div>
              </div>
              <table id="tester" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id Order</th>
                    <th>No Meja</th>
                    <th>Tanggal</th>
                    <th>Id User</th>
                    <th>Keterangan</th>
                    <th>Pemesanan</th>
                    <th>Status Order</th>
                    <th>Aksi</th>

                  </tr>
                </thead>
                <tbody>

                  <?php
                  $no = 1;
                  foreach($db->tampil_pesan() as $data){
                    ?>


                    <tr class="active">
                      <td><?php echo $no++; ?></center></td>
                      <td><?php echo $data['id_order']; ?></td>
                      <td><?php echo $data['no_meja']; ?></td>
                      <td><?php echo $data['tanggal']; ?></td>
                      <td><?php echo $data['id_user']; ?></td>
                      <td><?php echo $data['keterangan']; ?></td>
                      <td>
                        <a href="data_detail_order.php?id_order=<?php echo $data['id_order'];?>" button type="button" class="btn btn-primary btn-flat btn-pri"><i class="fa fa-plus" aria-hidden="true"></i> Detail</button>
</td>
                           <td>
                                         <?php
                                            if($data['status_order'] == 'Y')
                                            {
                                              ?>
                                            <a href="approveee.php?table=pesan&id_order=<?php echo $data['id_masakan']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                           Selesai
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approveee.php?table=pesan&id_order=<?php echo $data['id_masakan']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Verifikasi
                                            </a>
                                            <?php
                                            }
                                            ?>
                                       
                                        </td>
                      <td>
                        <a href="hapus_order.php?id_order=<?php echo $data['id_order'];?>" button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Delete</button></a>
                          <a href="edit_order.php?id_order=<?php echo $data['id_order'];?>"  button  class="btn btn-primary"><i class="fa fa-edit "></i> Edit</button></a>

                        </td></tr>
                        <?php
                      }
                      ?>

                    </tbody>
                  </table>

                  <div class="modal fade" id="tambah_user" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">      
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <div class="center">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                            <h1 class="page-title">Edit Order</h1>
                            <ul class="breadcrumb">
                            </ul>

                          </div>
                          <div class="main-content">

                            <div class="row">
                              <div class="col-md-6">
                                <br>
                                <form action="proses_pesan.php?aksi=tambah_pesan" method="post">
                                  <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane active in" id="home">
                                      <form id="tab">

                                        <div class="form-group">
                                          <label>Nomor Meja</label>
                                          <input type="text" name="no_meja" class="form-control">
                                        </div>
                                        <div class="form-group">
                                          <label>Tanggal</label>
                                          <input type="date" name="tanggal"  class="form-control">
                                        </div>
                                        <div class="form-group">
                                          <label>id_user</label>
                                          <input type="text" name="id_user" class="form-control">
                                        </div>
                                        <div class="form-group">
                                          <label>Keterangan</label>
                                          <input type="text" name="keterangan" class="form-control">
                                        </div>
                                        <div class="form-group">
                                          <label>Status Order</label>
                                          <select name="status_order" id="DropDownTimezone" class="form-control">
                                           <option>-Tidak Ada-</option>
                                           <option>Verifikasi</option>
                                           

                                      </select>
      </div>
</div>
   

    <div class="btn-toolbar list-toolbar">
      <button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      <a href="#myModal" data-toggle="modal" class="btn btn-danger">Batal</a>
    </div>

        </form>             
  </div>
</div></form></div></div></div></div></div>
      

                            <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Konfirmasi Batal</h3>
                                  </div>
                                  <div class="modal-body">
                                    <p class="error-text"><i class="fa fa-warning modal-icon"></i>Apakah anda yakin akan membatalkan?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                    <button class="btn btn-danger" data-dismiss="modal">Batal</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <br><br>
                            <br><br><br><br><br><br><br><br>
                            <br><br><br><br><br><br>
                            <br><br>
                            <br><br><br><br><br><br><br><br>
                            <br><br><br><br><br><br><br><br>


                            <?php
                            include "footer.php";
                            ?> 
