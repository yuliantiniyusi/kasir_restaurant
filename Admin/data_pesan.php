
<?php
include'database.php';
$db = new database();
?>
<?php
include 'header.php'
?>

<div class="content">
        <div class="header">
        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Order</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div><br>
             <div class="row"><br>
                   <div class="col-md-12">
                       <div class="white-box">
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No. Meja</th>
                                            <th>Tanggal</th>
                                            <th>Nama User</th>
                                            <th>Keterangan</th>
                                            <th>Keterangan Transaksi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                    <?php
                                    error_reporting(0);
                                    $no = 1;
                                    foreach($db->tampil_data_poder() as $x){
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['no_meja']; ?></td>
                                            <td><?php echo $x['tanggal']; ?></td>
                                            <td><?php echo $x['nama_user']; ?></td>
                                            <td><?php echo $x['keterangan']; ?></td>
                                            <td><?php
                                            if($x['keterangan_transaksi'] == 'N')
                                            {
                                              ?>
                                            <?php echo "Belum Terbayar";?></td><?php }?>
                                            <td> 
                                              <a href="detail_order.php?id_order=<?php echo $x['id_order'];?>" class="btn btn-primary btn-md">Detail</a>     
                                           </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                   </table>
                            </div>
                       </div>
                   </div>

               </div>
        <!-- End Wrapper-->
        </div>

        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
